import { createWebHashHistory, createRouter } from 'vue-router'
import AboutPage from './pages/AboutPage.vue'
import HomePage from './pages/HomePage.vue'
import ProductPage from './pages/ProductPage.vue'

export const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: '/about', name: 'about', component: AboutPage },
    { path: '', alias: '/home', name: 'home', component: HomePage },
    { path: '/product/:id', name: 'product', component: ProductPage }
  ]
})
